export type Country = {
    name: string,
    population: number,
    percentage: number
  }