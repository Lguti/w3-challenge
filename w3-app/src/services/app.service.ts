import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { API_HOST } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getCountries(query: string): Observable<any> {
    const url = `${API_HOST}/countries?name=${query}`;
    return this.http.get(url)
  }
}
