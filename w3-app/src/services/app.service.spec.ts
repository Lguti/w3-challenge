import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppService } from './app.service';
import { API_HOST } from '../constants';

describe('AppService', () => {
  let service: AppService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService]
    });
    service = TestBed.inject(AppService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should return countries from API', () => {
    const query = 'india';
    const dummyResponse = [{ name: 'India' }, { name: 'Indonesia' }];

    service.getCountries(query).subscribe(countries => {
      expect(countries).toEqual(dummyResponse);
    });

    const req = httpMock.expectOne(`${API_HOST}/countries?name=${query}`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyResponse);
  });

  it('should handle API error', () => {
    const query = 'invalid';
    const errorMessage = 'Error occurred while fetching countries';

    service.getCountries(query).subscribe({
      next: (value) => {
        fail('Expected an error to be thrown');
      },
      error: (err) => {
        expect(err.error).toEqual(errorMessage)
      },
    })

    const req = httpMock.expectOne(`${API_HOST}/countries?name=${query}`);
    expect(req.request.method).toBe('GET');
    expect(req.request.urlWithParams.split('?')[1].includes('name')).toBeTruthy;
    req.flush(errorMessage, { status: 500, statusText: 'Internal Server Error' });
  });
});
