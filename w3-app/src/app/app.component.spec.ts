import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppService } from '../services/app.service';
import { of, throwError } from 'rxjs';
import { Country } from '../types';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NO_CONTENT } from '../constants';

const countriesMock : Country[] = [
  {
      name: "India",
      population: 1409902000,
      percentage: 25.76
  },
  {
      name: "China",
      population: 1403426000,
      percentage: 25.64
  },
  {
      name: "Estados Unidos",
      population: 331800000,
      percentage: 6.06
  },
  {
    name: "Indonesia",
    population: 1403426000,
    percentage: 25.64
  }
]

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatFormFieldModule],
      declarations: [AppComponent],
      providers: [
        {
          provide: AppService,
          useValue: {
            getCountries: (query: string) => {
              if (query === 'valid') {
                return of(countriesMock);
              } else if (query.length === 2) {
                return of(null)
              } else {
                return throwError(() => new Error('Internal error'));
              }
            },
          },
        },
      ],
    });
    fixture = TestBed.createComponent(AppComponent)
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should initially have empty countries', () => {
    expect(component.countries).toEqual([]);
  });

  it('should set countries on button click with valid input', () => {
    component.inputValue = 'valid';
    component.onButtonClick();

    expect(component.loading).toBeTruthy;

    fixture.whenStable().then(() => {
      expect(component.loading).toBeFalsy;
      expect(component.filter).toBeTruthy;
      expect(component.errorResponse).toBe('');
      expect(component.countries).toEqual(countriesMock);
    });
  });

  it('should set countries and errorResponse on button click with 2 characters', () => {
    component.inputValue = 'qq';
    component.onButtonClick();

    expect(component.loading).toBeTruthy;

    fixture.whenStable().then(() => {
      expect(component.loading).toBeFalsy;
      expect(component.filter).toBeTruthy;
      expect(component.errorResponse).toBe(NO_CONTENT);
      expect(component.countries).toEqual([]);
    });
  });

  it('should handle error on button with invalid input', () => {
    component.inputValue = 'invalid';
    component.onButtonClick();

    expect(component.loading).toBeTruthy;

    fixture.whenStable().then(() => {
      expect(component.loading).toBeFalsy;
      expect(component.filter).toBeFalsy;
      expect(component.errorResponse).toBe('Internal error');
      expect(component.countries).toEqual([]);
    });
  });
});