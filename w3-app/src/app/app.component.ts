import { Component } from '@angular/core';
import { AppService } from '../services/app.service';
import { Country } from '../types/index';
import { NO_CONTENT } from '../constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private http: AppService) { }
  loading : boolean = false;
  title = 'w3-app';
  inputValue: string = '';
  countries: Country[] | [] = [];
  errorResponse: string = '';
  filter: boolean = false;
  displayedColumns: string[] = ['name', 'population', 'percentage'];

  onButtonClick() {
    this.loading = true;
    this.http.getCountries(this.inputValue).subscribe({
      next: (response: Country[] | null) => {
        this.filter = true;
        this.countries = response ? response : [];
        this.errorResponse = !response ? NO_CONTENT : ''
        this.loading = false;
      },
      error: (err) => {
        this.filter = false;
        this.countries = [];
        this.loading = false
        this.errorResponse = err.message;
      },
    });
  }
}