import { Column, Entity, ObjectId, ObjectIdColumn } from 'typeorm';
@Entity('Country')
export class Country {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  name: string;

  @Column()
  population: number;

  constructor(name: string, population: number) {
    this.name = name;
    this.population = population;
  }
}
