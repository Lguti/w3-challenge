import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Country } from '../entities/Country';
import { MongoRepository } from 'typeorm';

@Injectable()
export class CountryRepository {
  constructor(
    @InjectRepository(Country) private repository: MongoRepository<Country>,
  ) {}

  async findAll(): Promise<Country[]> {
    return await this.repository.find();
  }
}
