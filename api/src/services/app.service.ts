import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Country } from '../entities/Country';
import { CountryWithPercentage } from '../interfaces/CountryWithPercentage';
import { CountryRepository } from '../repositories/CountryRepository';

@Injectable()
export class AppService {
  constructor(
    @Inject('REPOSITORY_COUNTRY') private country: CountryRepository,
  ) {}

  async getCountries(query: string): Promise<CountryWithPercentage[]> {
    this.validateRequest(query);
    try {
      const countries = await this.country.findAll();
      const totalPopulation = countries.reduce(
        (acc, country) => acc + country.population,
        0,
      );

      const countriesWithInhabitantPercentage =
        this.getCountriesWithInhabitantPercentace(countries, totalPopulation);

      if (!query) {
        return countriesWithInhabitantPercentage;
      }

      // Return filtered countries with limit in 5
      return countriesWithInhabitantPercentage
        .filter((country) =>
          this.removeAccents(country.name)
            .toLowerCase()
            .includes(this.removeAccents(query.toLowerCase())),
        )
        .slice(0, 5);
    } catch (err) {
      throw new HttpException(
        'Internal error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  validateRequest(query: string): void {
    if (query && query.length < 3)
      throw new HttpException(
        'query string must be equal or higher than 3',
        HttpStatus.NO_CONTENT,
      );
  }

  getCountriesWithInhabitantPercentace(
    countries: Country[],
    totalPopulation: number,
  ): Array<any> {
    const response = countries.map((country) => {
      return {
        ...country,
        percentage: this.fixToDecimals(
          (country.population / totalPopulation) * 100,
        ),
      };
    });

    return response;
  }

  removeAccents(str: string): string {
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  fixToDecimals(percentage: number): number {
    return Math.floor(percentage * 100) / 100;
  }
}
