import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from './services/app.service';
import { Country } from './entities/Country';
import { CountryRepository } from './repositories/CountryRepository';
import { Types } from 'mongoose';

const ObjectIdMock = new Types.ObjectId();
const getNumberWithTwoDecimals = (num: number) => Math.floor(num * 100) / 100;

const countriesMock: Country[] = [
  {
    _id: ObjectIdMock,
    name: 'Test country 1',
    population: 1000,
  },
  {
    _id: ObjectIdMock,
    name: 'Test country 2',
    population: 2000,
  },
  {
    _id: ObjectIdMock,
    name: 'Test country 3',
    population: 3000,
  },
  {
    _id: ObjectIdMock,
    name: 'Test country 4',
    population: 4000,
  },
  {
    _id: ObjectIdMock,
    name: 'Test country 5',
    population: 5000,
  },
  {
    _id: ObjectIdMock,
    name: 'Test country 6',
    population: 6000,
  },
  {
    _id: ObjectIdMock,
    name: 'México',
    population: 7000,
  },
];

describe('AppService', () => {
  let appService: AppService;
  let countryRepository: CountryRepository;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        AppService,
        {
          provide: 'REPOSITORY_COUNTRY',
          useValue: {
            findAll: jest.fn(),
          },
        },
      ],
    }).compile();

    appService = app.get<AppService>(AppService);
    countryRepository = app.get<CountryRepository>('REPOSITORY_COUNTRY');
  });

  describe('getCountries', () => {
    it('should return all countries if there is no query string param', async () => {
      jest.spyOn(countryRepository, 'findAll').mockResolvedValue(countriesMock);

      const result = await appService.getCountries('');

      expect(result.length).toBe(7);
      expect(result.every((country) => country.percentage)).toBeTruthy();
    });

    it('should return countries with inhabitants percentage', async () => {
      jest
        .spyOn(countryRepository, 'findAll')
        .mockResolvedValue(countriesMock.slice(0, 2));

      const result = await appService.getCountries('');
      const [firstResult, secondResult] = result;

      expect(result.length).toBe(2);
      expect(firstResult.name).toBe('Test country 1');
      expect(getNumberWithTwoDecimals(firstResult.percentage)).toBe(33.33);
      expect(secondResult.name).toBe('Test country 2');
      expect(getNumberWithTwoDecimals(secondResult.percentage)).toBe(66.66);
    });

    it('should filter countries by query and return up to 5 results', async () => {
      jest.spyOn(countryRepository, 'findAll').mockResolvedValue(countriesMock);

      const result = await appService.getCountries('test');

      expect(result.length).toBe(5);
      expect(result[0].name).toBe('Test country 1');
      expect(result[1].name).toBe('Test country 2');
      expect(result[2].name).toBe('Test country 3');
      expect(result[3].name).toBe('Test country 4');
      expect(result[4].name).toBe('Test country 5');
    });

    it('should filter countries by query no matter if query has accent', async () => {
      jest.spyOn(countryRepository, 'findAll').mockResolvedValue(countriesMock);

      const result = await appService.getCountries('méxico');

      expect(result.length).toBe(1);
      expect(result[0].name).toBe('México');
    });

    it('should throw an exception (204 code) if the query is less than 3 characters', async () => {
      await expect(appService.getCountries('qq')).rejects.toThrowError(
        new HttpException(
          'query string must be equal or higher than 3',
          HttpStatus.NO_CONTENT,
        ),
      );
    });

    it('should throw an error if an error occurs in the repository', async () => {
      jest
        .spyOn(countryRepository, 'findAll')
        .mockRejectedValue(new Error('Repository error'));

      await expect(appService.getCountries('query')).rejects.toThrowError(
        new HttpException('Internal error', HttpStatus.INTERNAL_SERVER_ERROR),
      );
    });
  });
});
