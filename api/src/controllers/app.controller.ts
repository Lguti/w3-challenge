import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from '../services/app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/countries')
  public getcountries(@Query('name') query: string): object {
    return this.appService.getCountries(query);
  }
}
