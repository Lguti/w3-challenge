import { Country } from '../entities/Country';

export interface CountryWithPercentage extends Country {
  percentage: number;
}
