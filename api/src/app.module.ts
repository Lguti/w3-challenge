import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app.service';
import { Country } from './entities/Country';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CountryRepository } from './repositories/CountryRepository';

@Module({
  imports: [
    TypeOrmModule.forFeature([Country]),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_DATABASE,
      migrationsRun: true,
      entities: [Country],
      migrations: ['./dist/src/migrations/**/*{.ts,.js}'],
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: 'REPOSITORY_COUNTRY', useClass: CountryRepository },
  ],
})
export class AppModule {}
