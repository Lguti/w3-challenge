import { DataSource } from 'typeorm';

console.log(process.env.DB_DATABASE, process.env.DB_HOST);

export default new DataSource({
  type: 'mongodb',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE,
  migrationsRun: true,
  retryWrites: false,
  synchronize: false,
  logging: true,
  entities: ['./dist/src/entities/*/.ts'],
  migrations: ['./dist/src/migrations/*/{.ts,.js}'],
});
