# W3 Challenge :rocket: 

## You need at least ```node version 16``` for this project :cop: :warning:

## Root position
- #### **{{ROOT}}** -> **Is the principal folder of the project (by default w3-challenge)**

## Api position
- #### **{{ROOT}}/api**

## App position
- #### **{{ROOT}}/w3-app**

# For running ```w3-app``` and ```api``` you need the following steps
### Exec the command below on the root position **/**

- ``` docker-compose up -d --build ```

## Api  

- Running in **port 4000** 
- Base api url ```http://localhost:4000``` or ```http://127.0.0.1:4000```
- Get countries ```/countries```
- Get countries by query string ```/countries?name=ind``` **(return at maximum 5 countries)**

### Response json of ```/countries```
```
[
    {
        "name": "India",
        "population": 1409902000,
        "_id": "646d37484e18ecb14386bf9a",
        "percentage": 25.76
    },
    {
        "name": "China",
        "population": 1403426000,
        "_id": "646d37484e18ecb14386bf9b",
        "percentage": 25.64
    },
    {
        "name": "Estados Unidos",
        "population": 331800000,
        "_id": "646d37484e18ecb14386bf9c",
        "percentage": 6.06
    }...
]
```

### Response json of ```/countries?name=ind```
```
[
    {
        "name": "India",
        "population": 1409902000,
        "_id": "646d37484e18ecb14386bf9a",
        "percentage": 25.76
    },
    {
        "name": "Indonesia",
        "population": 1403426000,
        "_id": "646d37484e18ecb14386bf9b",
        "percentage": 25.64
    }
]
```

## w3-app
- **Running in port 80** 
- **Base url** ```http://localhost:80``` or ```http://127.0.0.1:80```

<hr>

# TEST
## Requirements steps
### 1 - Go to ```/api``` and exec the command below to install dependencies for ```api```

- ``` npm ci ``` or ```npm install```

### 1.1 - Exec the command below to run test for ```api``` on the api position **/api**

- ``` npm test ```

### 2 - Go to ```/w3-app``` and exec the command below to install dependencies for ```w3-app```

- ``` npm ci ``` or ```npm install```

### 2.1 - Exec the command below to run test for ```w3-app``` on the w3-app position **/w3-app**

- ``` npm test ```
